const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
var uglifycss = require('gulp-uglifycss');

gulp.task('writeHtml', function() {
	gulp.src('src/*.html')
		.pipe(gulp.dest('dist'))
});

gulp.task('imageMin', () =>
	gulp.src('src/images/*')
		.pipe(imagemin())
		.pipe(gulp.dest('dist/images'))
);

gulp.task('minifyJS', function() {
	gulp.src('src/js/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('dist/js'))
});

gulp.task('css', function () {
  gulp.src('src/css/*.css')
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('default', ['writeHtml', 'imageMin', 'minifyJS', 'css']);

gulp.task('watch', function() {
	gulp.watch('src/js/*.js', ['minifyJS']);
	gulp.watch('src/images/*', ['imageMin']);
	gulp.watch('src/*.html', ['writeHtml']);
	gulp.watch('src/css/*.css', ['css']);
})

