var clientImages = Array.from(document.getElementsByClassName('client'));
var annTestimonial = '<p class="testimonial-quote">Simply put, I read everything that Paul Tripp writes, I can\'t afford to miss one word</p><p class="testimonial-name">Ann Voskamp</p>'
var mattTestimonial = '<p class="testimonial-quote">When Paul Tripp teaches, preaches, or writes he does so through the lens of the gospel</p><p class="testimonial-name">Matt Chandler</p>'
var elyseTestimonial = '<p class="testimonial-quote">What I\'ve come to expect from Paul Tripp is consistently deep, transparent, biblical, wise, practical, gospel-driven counsel</p><p class="testimonial-name">Elyse Fitzpatrick</p>'
var testimonialContainer = document.getElementById('testimonial-box');
var videoButton = document.getElementById("video-button");
var videoSection = document.getElementById('video-section');
var bodyElement = document.querySelector('body');
var video = document.querySelector('#video-container iframe');
var paulTripp = document.getElementById('tripp-cutout');
var mobileTestimonial1 = testimonialContainer.cloneNode(true);
var mobileTestimonial2 = mobileTestimonial1.cloneNode(true);
var mobileTestimonial3 = mobileTestimonial1.cloneNode(true);

mobileTestimonial1.innerHTML = annTestimonial;
mobileTestimonial2.innerHTML = mattTestimonial;
mobileTestimonial3.innerHTML = elyseTestimonial;
var testimonialArray = [mobileTestimonial1, mobileTestimonial2, mobileTestimonial3];
/*Change Text on Hover of Image */

function changeText(e) {
  var currentSelectedTarget = document.querySelector('.client-selected');
  currentSelectedTarget.classList.remove('client-selected');
  e.currentTarget.classList.add('client-selected');
  if (e.currentTarget == clientImages[0]) {
  	testimonialContainer.innerHTML = annTestimonial;
  } else if (e.currentTarget == clientImages[1]) {
  	testimonialContainer.innerHTML = mattTestimonial;
  } else if (e.currentTarget == clientImages[2]) {
  	testimonialContainer.innerHTML = elyseTestimonial;
  }
}

/* Event Listener for Images */

clientImages.forEach(function(clientImage) {
	clientImage.addEventListener('mouseover', changeText);
});

/* Starting the video */

function startVideo() {
	bodyElement.setAttribute('style', 'height: 100%; overflow: hidden;');
	videoSection.style.display = 'block';
	video.setAttribute('src', 'https://player.vimeo.com/video/225462165?autoplay=1" width="800');
}

/* Event Listener for Video Button Click */

videoButton.addEventListener('click', startVideo);

/* Ending the Video */

function closeVideo(e) {
	if (e.target !== video) {
		console.log('working');
		videoSection.removeAttribute('style');
		bodyElement.removeAttribute('style');
		video.removeAttribute('src');
	}
}

/* Event Listener for Closing the Video */

videoSection.addEventListener('click', closeVideo);

window.addEventListener('resize', screenChange);

function screenChange() {
	if (window.innerWidth <= 768 ) {
		paulTripp.setAttribute('src', 'images/paultrippmobile.png');
		clientImages.forEach(function(clientImage, i) {
			clientImage.append(testimonialArray[i]);
		});
		console.log(fullTestimonialBoxes);
		fullTestimonialBoxes[0].parentNode.removeChild(fullTestimonialBoxes[0]);
	} else {
		var testimonialBoxes = Array.from(document.querySelectorAll('.client > .testimonial-boxes'));
		paulTripp.setAttribute('src', 'images/meetpaultrippw.png');
		testimonialBoxes.forEach(function(testimonialBox, i) {
			testimonialBox.parentNode.removeChild(testimonialBox);
		})
	}
}

screenChange();